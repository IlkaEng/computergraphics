﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Luz : MonoBehaviour {
    Light luzz;
    public Text textogen;

    float time = 0.0f;
	// Use this for initialization
	void Start () {
        luzz = GetComponent<Light>();
        luzz.enabled = !luzz.enabled;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if ((0f <= time) && (5f >= time))
        {
            luzz.enabled = false;
            textogen.text = "Light: " + luzz.enabled.ToString();
        }
        if (5f <= time)
        {
            luzz.enabled = true;
            textogen.text = "Light: " + luzz.enabled.ToString();
        }
	}
}
