﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TextOnScreen : MonoBehaviour {
    Examen obj;
    public Text textogen;
    string info;
    float time;

    Light lighto;
    
	// Use this for initialization
	void Start () {
        textogen.text = "Light: " + info;
        time = 0.0f;
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if ((0f <= time) && (5f >= time))
        {
            info = lighto.enabled.ToString();
        }
    }
}
