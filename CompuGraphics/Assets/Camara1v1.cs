﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara1v1 : MonoBehaviour {
    public Transform targetTransform;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, targetTransform.position, Time.deltaTime * .4f);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetTransform.rotation, Time.deltaTime);
    }
}
