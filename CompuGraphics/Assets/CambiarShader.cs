﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarShader : MonoBehaviour {
    Renderer rend;
	// Use this for initialization
	void Start () {
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update () {
        float Amb = Mathf.PingPong(Time.time, 1.0F);
        rend.material.SetFloat("_Color", Amb);
    }
}
