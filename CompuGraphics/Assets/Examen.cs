﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Examen : MonoBehaviour {
    float time = 0.0f;
    public Shader shaderdiffuse;
    public Shader shaderTexture;
    public Renderer rend;
    public Shader shaderNormalMap;
    public Text textoshader;
    public Text textocolor;
    public Text textotimer;
    public Text textotexture;



    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        shaderdiffuse = Shader.Find("Custom/DiffuseShader");
        shaderTexture = Shader.Find("Custom/ScrollingShader");
        shaderNormalMap = Shader.Find("Custom/ScrollNormalMap");

    }

    // Update is called once per frame
    void Update () {
        time += Time.deltaTime;
        if ((0f <= time) && (10f >= time))
        {
            rend.material.shader = shaderdiffuse;
            rend.material.SetColor("_Color", Color.black);
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textocolor.text = "Color: " + rend.material.color.ToString();
            textotimer.text = "Timer: " + time.ToString();
            textotexture.text = "Texture: " + "OFF";
        }
        if ((10f <= time) && (15 >= time) )
        {
            float Int = Mathf.PingPong(Time.time, 5f);
            rend.material.SetColor("_Color", Color.Lerp(Color.black,Color.magenta, Int));
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textocolor.text = "Color: " + rend.material.color.ToString();
            textotimer.text = "Timer: " + time.ToString();
            textotexture.text = "Texture: " + "OFF";

        }
        if ((15f <= time) && (20 >= time))
        {
            rend.material.shader = shaderTexture;
            rend.material.mainTexture = Resources.Load("EXO") as Texture2D;
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textocolor.text = "Color: " + rend.material.color.ToString();
            textotimer.text = "Timer: " + time.ToString();
            textotexture.text = "Texture: " + rend.material.mainTexture.ToString();

        }
        if ((20f <= time) && (25 >= time))
        {
            rend.material.shader = shaderNormalMap;
            rend.material.mainTexture = null;
            rend.material.SetTexture("_NormalTex", (Resources.Load("NormalMap") as Texture2D));
            rend.material.SetColor("_Color", Color.white);
            textocolor.text = "Color: " + rend.material.color.ToString();
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textotimer.text = "Timer: " + time.ToString();
            textotexture.text = "Texture: " + "OFF";

        }
        if ((25f <= time) && (30 >= time))
        {
            rend.material.mainTexture = (Resources.Load("ROCK") as Texture2D);
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textocolor.text = "Color: " + rend.material.color.ToString();
            textotimer.text = "Timer: " + time.ToString();
            textotexture.text = "Texture: " + rend.material.mainTexture.ToString();
        }
        if ((30f <= time) && (60 >= time))
        {
            textoshader.text = "Shader: " + rend.material.shader.name.ToString();
            textocolor.text = "Color: " + rend.material.color.ToString();
            textotimer.text = "Timer: " + time.ToString();
            //textotexture.text = "Texture: " + rend.material.mainTexture.ToString();

        }

        print(time);
	}
}
