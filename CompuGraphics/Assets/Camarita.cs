﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camarita : MonoBehaviour {
    Camera camaraa;
    float time = 0f;
    int speed;

	// Use this for initialization
	void Start () {
        camaraa = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if ((30f <= time) && (55 >= time))
        {
            float mov = Mathf.PingPong(Time.time, .0018F);
            camaraa.transform.Translate(new Vector3(0, 0, mov));
        }
            

    }
}
